
set clipboard=unnamed
syntax on
set autoread
set tabstop=2
set shiftwidth=2
set relativenumber
set number
set expandtab
set cmdheight=2
set nocompatible              " be iMproved, required
filetype plugin on

" code folding
set foldmethod=indent
set foldnestmax=10
set nofoldenable
set foldlevel=2

nmap nt :NERDTree<enter>
nmap nf :NERDTreeFind<CR><enter>
nmap <Leader>r :NERDTreeFocus<cr> \| R \| <c-w><c-p>
" basic left/right window hopping remap
nmap <c-h> <c-w>h
nmap <c-l> <c-w>l

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'neoclide/coc.nvim', {'branch': 'release'}

Plugin 'preservim/nerdtree.git'
Plugin 'Xuyuanp/nerdtree-git-plugin'

Plugin 'tpope/vim-surround'

" Linting / Prettier
Plugin 'editorconfig/editorconfig-vim.git'
Plugin 'prettier/vim-prettier'
Plugin 'w0rp/ale'

" Javascript plugins
Plugin 'pangloss/vim-javascript'
Plugin 'mxw/vim-jsx'
Plugin 'drewtempelmeyer/palenight.vim'

Plugin 'tpope/vim-commentary'
Plugin 'xolox/vim-misc'
Plugin 'xolox/vim-notes'

" Python plugins
Plugin 'vim-python/python-syntax'

" GDScript plugins
Plugin 'calviken/vim-gdscript3'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
" 
 
" use <tab> for trigger completion and navigate to the next complete item
function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~ '\s'
endfunction

inoremap <silent><expr> <Tab>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<Tab>" :
      \ coc#refresh()

let g:NERDTreeGitStatusWithFlags = 1

set termguicolors

set background=dark
colorscheme palenight
let g:airline_theme = "palenight"

autocmd StdinReadPre * let g:isReadingFromStdin = 1
autocmd VimEnter * if !argc() && !exists('g:isReadingFromStdin') | NERDTree | endif

" coc config
let g:coc_global_extension = [
  \ 'coc-snippets',
  \ 'coc-pairs',
  \ 'coc-eslint',
  \ 'coc-prettier',
  \ 'coc-json',
  \ ]

" FORMATTERS
let g:ale_fixers = {
\ 'javascript': ['eslint'],
\}
let g:ale_fix_on_save = 1
let g:ale_sign_error = '✘'
let g:ale_sign_warning = '⚠'
highlight ALEErrorSign ctermbg=NONE ctermfg=red
highlight ALEWarningSign ctermbg=NONE ctermfg=yellow

set backspace=indent,eol,start

let &t_SI.="\e[5 q" "SI = INSERT mode
let &t_EI.="\e[1 q" "EI = NORMAL mode (ELSE)
